const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(body.urlencoded(express.static('public')));
app.use(cookie());
app.use(session({ secret: 'Passw0rd' }));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '1234',
    port: 3306,
    database: 'f2k3'
}, 'single'));
const routeLogin = require('./routes/routeLogin');
app.use('/', routeLogin);

const rout = require('./routes/homeRoute');
app.use('/', rout);

const Pontakorn = require('./routes/s611998030Route');
app.use('/', Pontakorn);


const s611998025Rot =require('./routes/s611998025Route');
app.use('/',s611998025Rot);

const s611998032Rot =require('./routes/s611998032Route');
app.use('/',s611998032Rot);

const route = require('./routes/s611998039Route');
app.use('/', route);


app.listen('8081');
