const express = require('express');
const router = express.Router();
const studentToller = require('../controllers/s611998032Controller');
const validator = require('../controllers/s611998032Validator');

router.get('/s611998032',studentToller.list);
router.post('/s611998032/add',validator.stuValidator,studentToller.add);
router.get('/s611998032/deleteNow/:id',studentToller.deleteNow);
router.get('/s611998032/delete/:id',studentToller.delete);
router.get('/s611998032/update/:id',studentToller.edit);
router.post('/s611998032/update/:id',validator.stuValidator,studentToller.update);
router.get('/s611998032/new',studentToller.new);

module.exports = router;  