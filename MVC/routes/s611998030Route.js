const express = require('express');
const router = express.Router();

const fordController = require('../controllers/s611998030Controller');
const validator = require('../controllers/s611998030Validator')

router.get('/pontakorn',fordController.list);

router.post('/pontakorn/add',validator.pontakornValidator,fordController.add);

router.get('/pontakorn/deleteNow/:id',fordController.deleteNow);

router.get('/pontakorn/delete/:id',fordController.delete);

router.get('/pontakorn/update/:id',validator.pontakornValidator,fordController.edit);

router.post('/pontakorn/update/:id',validator.pontakornValidator,fordController.update);

router.get('/pontakorn/new',fordController.new);


module.exports = router;
