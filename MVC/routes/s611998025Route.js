const express = require('express');
const router = express.Router();
const studentToller = require('../controllers/s611998025Controller');
const validator = require('../controllers/s611998025Validator');

router.get('/s611998025',studentToller.list);
router.post('/s611998025/add',validator.stuValidator,studentToller.add);
router.get('/s611998025/deleteNow/:id',studentToller.deleteNow);
router.get('/s611998025/delete/:id',studentToller.delete);
router.get('/s611998025/update/:id',studentToller.edit);
router.post('/s611998025/update/:id',validator.stuValidator,studentToller.update);
router.get('/s611998025/new',studentToller.new);

module.exports = router;  