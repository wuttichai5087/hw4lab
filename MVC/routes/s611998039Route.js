const express = require('express');
const router = express.Router();
const studentToller = require('../controllers/s611998039Controller');
const validator = require('../controllers/s611998039Validator');

router.get('/s611998039',studentToller.list);
router.post('/s611998039/add',validator.addValidator,studentToller.add);
router.get('/s611998039/deleteNow/:id',studentToller.deleteNow);
router.get('/s611998039/delete/:id',studentToller.delete);
router.get('/s611998039/update/:id',studentToller.edit);
router.post('/s611998039/update/:id',validator.addValidator,studentToller.update);
router.get('/s611998039/new',studentToller.new);

module.exports = router;
