const express = require('express');
const router = express.Router();

const cController = require('../controllers/controller');

router.get('/',cController.loginForm);
router.post('/login',cController.login);
router.get('/homeConnect',cController.homeForm);
router.get('/home',cController.home);
router.get('/logout',cController.logout);
module.exports = router;
