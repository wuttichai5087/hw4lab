const controller = {};

controller.loginForm = (req, res) => {
    res.render("loginform", { session: req.session });
};

controller.login = (req, res) => {
  if(req.body.username == 'ce' && req.body.password == 'mirot'){
    req.session.user=req.body.username;
      res.redirect('/home');
    console.log('ok');
  }else{
    //res.end('login Error')
    res.render("loginFail");
  }
};

controller.homeForm = (req, res) => {
  if(req.session.user){
    res.render("home", { session: req.session });
  }else {
    res.redirect('/');
  }
};
controller.home = (req, res) => {
    res.render("home", { session: req.session });
};

controller.logout = (req, res) => {
  req.session.destroy();
  res.redirect('/');
};

module.exports = controller;
