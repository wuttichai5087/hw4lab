const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT s.id as ids,s.win swin, s.kannoi skannoi, s.D611998025 sD611998025,t.id as idt, t.ford tford, t.changpat tchangpat, t.D611998030 tD611998030  FROM tanawin s left join pontakorn t on s.id_pontakorn = t.id',(err,studentData) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998025/listForm',{session: req.session,data:studentData});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
    const data=req.body;
    if(data.id_pontakorn==""){
      data.id_pontakorn=null;
    }
    const errors = validationResult(req);
    if(req.session.user){
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998025/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO tanawin set ?',[data],(err,studentData)=>{
            res.redirect('/s611998025');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.delete = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('SELECT s.id as ids,s.win swin, s.kannoi skannoi, s.D611998025 sD611998025,t.id as idt, t.ford tford, t.changpat tchangpat, t.D611998030 tD611998030  FROM tanawin s left join pontakorn t on s.id_pontakorn = t.id HAVING ids = ?',[id],(err,studentData)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998025/deleteForm',{session: req.session,data:studentData[0]});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM tanawin WHERE id= ?',[id],(err,studentData)=>{
            if(err){
                req.session.errors=errorss;
                req.session.success=false;
            }
            else{
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(studentData);
            res.redirect('/s611998025');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM tanawin WHERE id= ?',[id],(err,studentData)=>{
                    conn.query('SELECT * FROM pontakorn',(err,pontakornData)=>{res.render('s611998025/updateForm',{
                        session: req.session,
                        data1:studentData[0],
                        data2:pontakornData});
                    });
                });
            });
        }else {
            res.redirect('/');
          }
        };

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_pontakorn==""){
      data.id_pontakorn=null;
    }
    if(req.session.user){
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM tanawin WHERE id= ?',[id],(err,studentData)=>{
                    conn.query('SELECT * FROM pontakorn',(err,pontakornData)=>{res.render('s611998025/updateForm',{
                        session: req.session,
                        data1:studentData[0],
                        data2:pontakornData});
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  tanawin SET ?  WHERE id = ?',[data,id],(err,studentData) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998025');
               });
             });
        }
}else {
    res.redirect('/');
  }
};

controller.new = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM pontakorn',(err,pontakornData) =>{
        res.render('s611998025/addForm',{data:pontakornData,session: req.session});
      });
    });
  }else {
      res.redirect('/');
    }
  };


module.exports = controller;
