const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT s.id as ids,s.arm sarm, s.mungrienthong smungrienthong, s.D611998032 sD611998032,t.id as idt, t.win twin, t.kannoi tkannoi, t.D611998025 tD611998025  FROM witshayut s left join tanawin t on s.id_tanawin = t.id',(err,s6132) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998032/listForm',{session: req.session,data:s6132});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
    const data=req.body;
    if(data.id_tanawin==""){
      data.id_tanawin=null;
      console.log(data.id_tanawin);
    }
    console.log(data);
    const errors = validationResult(req);
    if(req.session.user){
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998032/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO witshayut set ?',[data],(err,s6132)=>{
            res.redirect('/s611998032');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.delete = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('SELECT s.id as ids,s.arm sarm, s.mungrienthong smungrienthong, s.D611998032 sD611998032,t.id as idt, t.win twin, t.kannoi tkannoi, t.D611998025 tD611998025  FROM witshayut s left join tanawin t on s.id_tanawin = t.id HAVING ids=?',[id],(err,s6132)=>{
          conn.query('SELECT * FROM tanawin',(err,studentData)=>{res.render('s611998032/deleteForm',{
              session: req.session,
              data:s6132[0],
              data1:studentData[0]});
          });
        });
    });
}else {
    res.redirect('/');
  }
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM witshayut WHERE id= ?',[id],(err,s6132)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
                 req.session.errors=errorss;
                 req.session.success=false;
            }else {
              req.session.success=true;
              req.session.topic="ลบข้อมูลเสร็จแล้ว";

            }
            console.log(s6132);
            res.redirect('/s611998032');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM witshayut WHERE id= ?',[id],(err,s6132)=>{
                    conn.query('SELECT * FROM tanawin',(err,studentData)=>{res.render('s611998032/updateForm',{
                        session: req.session,
                        data1:s6132[0],
                        data2:studentData});
                    });
                });
            });
        }else {
            res.redirect('/');
          }
        };

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_tanawin==""){
      data.id_tanawin=null;

      console.log(data.id_tanawin);
    }
    if(req.session.user){
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM witshayut WHERE id= ?',[id],(err,s6132)=>{
                    conn.query('SELECT * FROM tanawin',(err,studentData)=>{res.render('s611998032/updateForm',{
                        session: req.session,
                        data1:s6132[0],
                        data2:studentData});
                    });
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  witshayut SET ?  WHERE id = ?',[data,id],(err,s6132) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998032');
               });
             });
        }
}else {
    res.redirect('/');
  }
};

controller.new = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM tanawin',(err,studentData) =>{
        res.render('s611998032/addForm',{data:studentData,session: req.session});
      });
    });
  }else {
      res.redirect('/');
    }
  };


module.exports = controller;
