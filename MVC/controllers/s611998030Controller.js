const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT p.id as idp,p.ford pford, p.changpat pchangpat, p.D611998030 pD611998030,w.id as idw, w.wut wwut, w.saehan wsaehan, w.D611998039 wD611998039  FROM pontakorn p left join wuttichai w on p.id_wuttichai = w.id',(err,pontakornData) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998030/listForm',{session: req.session,data:pontakornData});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
    if(data.id_wuttichai == ""){
       data.id_wuttichai = null;
    }
    if(req.session.user){
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/pontakorn/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO pontakorn set ?',[data],(err,pontakornData)=>{
            res.redirect('/pontakorn');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.delete = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('SELECT p.id as idp, p.ford pford, p.changpat pchangpat, p.D611998030 pD611998030,w.id as idw, w.wut wwut, w.saehan wsaehan, w.D611998039 wD611998039  FROM pontakorn p left join wuttichai w on p.id_wuttichai = w.id HAVING p.id= ?',[id],(err,pontakornData)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998030/deleteForm',{session: req.session,data:pontakornData[0]});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM pontakorn WHERE id= ?',[id],(err,pontakornData)=>{
            if(err){
                const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
req.session.errors=errorss;
req.session.success=false;
            }else{
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(pontakornData);
            res.redirect('/pontakorn');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM pontakorn WHERE id= ?',[id],(err,pontakornData)=>{
                    conn.query('SELECT * FROM wuttichai',(err,wuttichaiData)=>{res.render('s611998030/updateForm',{
                        session: req.session,
                        data1:pontakornData[0],
                        data2:wuttichaiData});
                    });
                });
            });
        }else {
            res.redirect('/');
          }
        };

          controller.update = (req,res) => {
            const errors = validationResult(req);
            const { id } = req.params;
            const data = req.body;
            if(data.id_wuttichai == ""){
                data.id_wuttichai = null;
             }
            if(req.session.user){
            console.log(data);
                if(!errors.isEmpty()){
                    req.session.errors=errors;
                    req.session.success=false;
                    req.getConnection((err,conn)=>{
                        conn.query('SELECT * FROM pontakorn WHERE id= ?',[id],(err,pontakornData)=>{
                            conn.query('SELECT * FROM wuttichai',(err,wuttichaiData)=>{
                            if(err){
                                res.json(err);
                            }
                            res.render('s611998030/updateForm',{
                                session: req.session,
                                data1:pontakornData[0],
                                data2:wuttichaiData});
                            })
                        });
                    });
                }else{
                    req.session.success=true;
                    req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
                    req.getConnection((err,conn) => {
                        conn.query('UPDATE  pontakorn SET ?  WHERE id = ?',[data,id],(err,studentData) => {
                          if(err){
                              res.json(err);
                          }
                       res.redirect('/pontakorn');
                       });
                     });
                }
        }else {
            res.redirect('/');
          }
        };


controller.new = (req,res) => {
    if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM wuttichai',(err,wuttichai) =>{
        res.render('s611998030/addform',{data:wuttichai,session: req.session});
      });
    });
  }else {
    res.redirect('/');
  }
};


module.exports = controller;
