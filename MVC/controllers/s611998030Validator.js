const { check } = require('express-validator');
exports.pontakornValidator = [
    check('ford',"บรรทัดที่ 1 ไม่ถูกต้อง !").not().isEmpty(),
    check('changpat',"บรรทัดที่ 2 ไม่ถูกต้อง !").isFloat(),
    check('D611998030',"บรรทัดที่ 3 ไม่ถูกต้อง !").isInt()];
