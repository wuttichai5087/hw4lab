const { check } = require('express-validator');
/*
exports.addValidator = [  check('wut',"WUT ไม่ถูกต้อง !").not().isEmpty(),
                          check('saehan',"SAEHAN ไม่ถูกต้อง !").isFloat(),
                          check('D611998039',"D611998039 ถูกต้อง !").not().isEmpty(),
                          check('id_witshayut',"ไอดีไม่ถูกต้อง !").not().isEmpty()];
                          */

exports.addValidator = [
    check('wut',"WUT ไม่ถูกต้อง !").not().isEmpty(),
    check('saehan',"SAEHAN ไม่ถูกต้อง !").isFloat(),
    check('D611998039',"D611998039 ถูกต้อง !").isInt()];
