const { check } = require('express-validator');
exports.stuValidator = [
    check('win',"1 !").not().isEmpty(),
    check('kannoi',"2 !").isFloat(),
    check('D611998025',"3 !").isInt()];
