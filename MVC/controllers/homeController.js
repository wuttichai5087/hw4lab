const controller = {};
const { validationResult } = require('express-validator');

controller.log = function(req,res){
    res.render("../views/home",{session:req.session});
}

module.exports = controller;
