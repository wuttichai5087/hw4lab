const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) =>{
        conn.query('SELECT w.id as ids,w.wut wwut, w.saehan wsaehan, w.D611998039 wD611998039,t.id as idt, t.arm tarm, t.mungrienthong tmungrienthong, t.D611998032 tD611998032  FROM wuttichai w left join witshayut t on w.id_witshayut = t.id',(err,studentData) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998039/listForm',{session: req.session,data:studentData});
        });
    });
}else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
    const data=req.body;
     if(data.id_witshayut==""){
       data.id_witshayut=null;
     }
    if(req.session.user){
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998039/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO wuttichai set ?',[data],(err,studentData)=>{
            res.redirect('/s611998039');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.delete = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('SELECT w.id as ids,w.wut wwut, w.saehan wsaehan, w.D611998039 wD611998039,t.id as idt, t.arm tarm, t.mungrienthong tmungrienthong, t.D611998032 tD611998032  FROM wuttichai w left join witshayut t on w.id_witshayut = t.id HAVING w.id = ?',[id],(err,studentData)=>{
          if(err){
              res.json(err);
          }
          res.render('s611998039/deleteForm',{session: req.session,data:studentData[0]});
      });
  });
}else {
    res.redirect('/');
  };
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM wuttichai WHERE id= ?',[id],(err,studentData)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(studentData);
            res.redirect('/s611998039');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM wuttichai WHERE id= ?',[id],(err,studentData)=>{
                    conn.query('SELECT * FROM witshayut',(err,teacherData)=>{res.render('s611998039/updateForm',{
                        session: req.session,
                        data1:studentData[0],
                        data2:teacherData});
                    });
                });
            });
        }
      }

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.id_witshayut==""){
      data.id_witshayut=null;
    }
    if(req.session.user){
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM wuttichai WHERE id= ?',[id],(err,studentData,teacherData)=>{
                    conn.query('SELECT * FROM witshayut',(err,teacherData)=>{
                    if(err){
                        res.json(err);
                    }

                    res.render('s611998039/updateForm',{session: req.session,data1:studentData[0],data2:teacherData});

                })
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  wuttichai SET ?  WHERE id = ?',[data,id],(err,studentData) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998039');
               });
             });
        }
}else {
    res.redirect('/');
  }
};

controller.new = (req,res) => {
  if(req.session.user){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM witshayut',(err,teacher) =>{
        res.render('s611998039/addForm',{data:teacher,session: req.session});
      });
    });
  }else {
      res.redirect('/');
    }
  };


module.exports = controller;
